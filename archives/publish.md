# npm public

设置仓库地址为npm官方仓库地址(国内大部分都使用阿里淘宝镜像，如果没改publish会失败)

```sh
npm config set proxy null
npm config set https-proxy null
npm config set registry https://registry.npmjs.org/
```

```sh
npm login
npm publish
```

<!-- npm publish dist -->
