# rollup.js 中文文档
>https://www.rollupjs.com/


## 一、安装 Rollup

```sh
# 本地安装
npm install --save rollup
npx rollup -v
```

```sh
# 安装插件
npm install --save-dev @rollup/plugin-alias           # 在代码中使用别名来引用模块
npm install --save-dev @rollup/plugin-terser          # 压缩代码
npm install --save-dev @rollup/plugin-commonjs        # 压缩代码
npm install --save-dev @rollup/plugin-typescript      # 支持对TypeScript代码的打包
npm install --save-dev @rollup/plugin-node-resolve    # 解析第三方模块的依赖关系

npm install --save-dev rollup-plugin-dts

@rollup/plugin-babel    # 转换 ES6+ 代码为 ES5
@rollup/plugin-buble    # 转换 ES6+ 代码为 ES5

rollup-plugin-dts
```


```sh
# 其他
tslib        # 如果在tsconfig中配置了lib就需要使用tslib依赖
@types/node  # 
```


## 二、格式

- amd - 异步模块定义，适用于 RequireJS 等模块加载器
- cjs - CommonJS，适用于 Node 环境和其他打包工具（别名：commonjs）
- es - 将 bundle 保留为 ES 模块文件，适用于其他打包工具以及支持 <script type=module> 标签的浏览器（别名: esm，module）
- iife - 自执行函数，适用于 <script> 标签。（如果你要为你的应用创建 bundle，那么你很可能用它。）
- umd - 通用模块定义，生成的包同时支持 amd、cjs 和 iife 三种格式
- system - SystemJS 模块加载器的原生格式（别名: systemjs） 

UMD and IIFE output formats are not supported for code-splitting builds
