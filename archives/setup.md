

```sh
# rollup
npm install --save-dev rollup
# "rollup-plugin-glslify": "1.2.0",
# "rollup-plugin-uglify": "^6.0.4",
# "rollup-plugin-web-worker-loader": "^1.3.0",


npm install --save-dev @rollup/plugin-alias
npm install --save-dev @rollup/plugin-buble
npm install --save-dev @rollup/plugin-commonjs
npm install --save-dev @rollup/plugin-json
npm install --save-dev @rollup/plugin-multi-entry
npm install --save-dev @rollup/plugin-node-resolve
npm install --save-dev @rollup/plugin-terser
npm install --save-dev @rollup/plugin-replace
npm install --save-dev @rollup/plugin-typescript


```

```sh
# misc
npm install --save-dev rimraf
npm install --save-dev cross-env
npm install --save-dev npm-run-all
npm install --save-dev tslib
npm install --save-dev ts-node
```



```json
  "devDependencies": {
    "buffer": "^5.6.0",
    "@babel/cli": "^7.13.10",
    "@babel/core": "^7.13.10",
    "@babel/node": "^7.13.12",
    "@babel/helper-define-map": "^7.13.12",
    "@babel/plugin-syntax-jsx": "^7.12.13",
    "@babel/plugin-external-helpers": "^7.12.13",
    "@babel/plugin-proposal-class-properties": "^7.13.0",
    "@babel/plugin-proposal-decorators": "^7.13.5",
    "@babel/plugin-proposal-object-rest-spread": "^7.13.8",
    "@babel/plugin-syntax-dynamic-import": "^7.8.3",
    "@babel/plugin-transform-proto-to-assign": "^7.13.12",
    "@babel/preset-env": "^7.13.12",
    "@babel/preset-typescript": "^7.7.4",
    "@babel/types": "^7.7.4",
    "@types/jest": "^24.0.23",
    "@types/node": "^12.12.14",
    "babel-jest": "^24.9.0",
    "babel-loader": "^8.0.6",
    "babel-plugin-inline-import": "^3.0.0",
    "coveralls": "^3.0.9",
    "cz-conventional-changelog": "^3.0.2",
    "husky": "^3.1.0",
    "jest": "^24.9.0",
    "jest-config": "^24.9.0",
    "lerna": "^3.19.0",
    "lint-staged": "^9.5.0",
    "npm-run-all": "^4.1.5",
    "prettier": "^1.19.1",
    "semantic-release": "^15.13.31",
    "stylelint": "^12.0.0",
    "stylelint-config-prettier": "^8.0.0",
    "stylelint-config-standard": "^19.0.0",
    "stylelint-declaration-block-no-ignored-properties": "^2.2.0",
    "travis-deploy-once": "^5.0.11",
    "ts-jest": "^24.2.0",
    "ts-node": "^8.5.4",
    "tslib": "^1.10.0",
    "tslint": "^5.20.1",
    "tslint-config-airbnb": "^5.11.2",
    "tslint-config-prettier": "^1.18.0",
    "tslint-plugin-prettier": "^2.0.1",
    "typedoc": "^0.15.3",
    "typescript": "3.7.3",
    "highlight.js": "^9.18.1",
    "less": "^3.11.1",
    "file-loader": "^5.1.0",
    "less-loader": "^5.0.0",
    "markdown-it-container": "^2.0.0",
    "child_process": "^1.0.2",
    "debug": "^2.6.3",
    "fs-extra": "^3.0.1",
    "nodemon": "^1.12.5",
    "pm2": "^2.9.2",
    "axios": "^0.19.2",
    "moment": "^2.24.0"
  },

```