# TS + Rollup 工具函数库环境搭建

>https://cloud.tencent.com/developer/article/2061463


## 预期目标

- [1. 实现一个 javascript 工具函数库](#一构建目录结构)
- [2. 支持 typescript](#二安装-typescript-环境)
- [3. 打包构建工具采用 rollup](#三初始化-rollup-打包环境)
- [4. eslint 校验](#4-eslint-配置)
- [5. prettier 代码自动格式化](#5-prettier-代码自动格式化)
- [6.团队协作 commit message 格式约束](#husky-git-提交约束)
- [其他](#其他)
- [issue](#issue)
- IDE 采用 vscode

## 一、构建目录结构

首先就是设计并搭建项目的目录结构，并初始化一个 package.json 文件，用于描述当前项目的功能。

```shell
mkdir demo
mkdir demo/src
cd demo
npm init -y       # 生成 package.json
```

目录结构

```sh
archive
example
roolup
src
tsconfig
```

在 package.json 中设置如下:

```json
// package.json
{
    "type": "module",
    "main": "./dist/index.cjs.js",
    "module": "./dist/index.esm.js",
    "types": "./dist/index.d.ts",
    "files": ["dist"]
  "scripts": {
    "clean": "rimraf dist",
    "dev": "run-p clean dev:01",
    "dev:01": "rollup --config rollup/rollup.config.js"
    "test": "echo \"Error: no test specified\" && exit 1",
  },
}
{
  "name": "meteojs",
  "version": "1.0.0",
  "description": "",
  "author": "shenyc",
  "license": "ISC",
  "type": "module",
  "main": "./dist/index.cjs.js",
  "module": "./dist/index.esm.js",
  "types": "./dist/index.d.ts",
  "files": ["dist"],
  "keywords": [],
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "rollup --config rollup.config.js"
  }
}

```

files 字段是用于约定在发包的时候 npm 会发布包含的文件和文件夹。

## 二、安装 TypeScript 环境

全局安装 TypeScript 以及 tslib 库，并初始化得到 tsconfig.json

```sh
# 全局安装
npm install --global typescript tslib
tsc --init    # 生成 tsconfig.json
```

如果愿意也可把 TypeScript 安装在本地, 本地安装时初始化 tsconfig.json 需使用 npm 格式

```sh
# 本地安装
npm install --save-dev typescript tslib
npx tsc --init    # 生成 tsconfig.json
```

默认的 tsconfig.json 配置需要修改，以支持我们能够编译成 ES 模块。  
第一次修改如下：

```json
// tsconfig.json
{
    "target": "esnext", 
    "module": "esnext",
    "moduleResolution": "node",
    "esModuleInterop": true, 
    "forceConsistentCasingInFileNames": true,
    "strict": true,
    "skipLibCheck": true,
    // 输出 *.d.ts
    "declaration": true,
    "outDir": "dist",
    "declarationDir": "dist/types",
}
```

## 三、初始化 Rollup 打包环境

选择 Rollup 来作为打包工具，Rollup 较好地支持 tree shaking，使得打包出来的包体积更小。

### 3.1 安装依赖

```sh
npm install --save-dev rollup
npm install --save-dev @rollup/plugin-json            # 允许 Rollup 从 JSON 文件导入数据
npm install --save-dev @rollup/plugin-alias           # 在代码中使用别名来引用模块 (@ => src)
npm install --save-dev @rollup/plugin-terser          # 压缩代码
npm install --save-dev @rollup/plugin-commonjs        # ？
npm install --save-dev @rollup/plugin-typescript      # 支持对 TS 代码的打包
npm install --save-dev @rollup/plugin-node-resolve    # 解析第三方模块的依赖关系
npm install --save-dev rollup-plugin-dts              # 配置 tsconfig.json 即可实现 *.d.ts
```

### 3.2 配置 rollup.config.js

在根路径下新建 rollup.config.js

```js
// rollup.config.js
import json from "@rollup/plugin-json";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "@rollup/plugin-typescript";
import nodeResolve from "@rollup/plugin-node-resolve";
import { defineConfig } from "rollup";

export default defineConfig([
    {
        input: 'src/index.ts',
        output: [
            {
                file: 'dist/index.cjs.js',
                format: 'cjs',
            },
        ],
        plugins: [
            typescript({ tsconfig: './tsconfig.json' }),
        ],
        treeshake: true,
    },
]);
```

### 3.3 修改 scripts

来到 package.json 文件中，为了使用快捷指令，以及调用 rollup 作为开发的预览功能，需要配置 scripts 字段如下：

```json
"scripts": {
  "dev": "rollup -wm -c",
  "build": "rollup -c",
  "test": "echo \"Error: run tests from root\" && exit 1"
}
```

如此，我们便可以通过 npm run dev，就能在开发的时候实时编译。

如果需要打包，则先执行 npm run build 命令即可完成打包。

## 4 eslint 配置

如果是小团队协作开发，就会涉及到代码规范问题，因此将在项目中引入 eslint。

```sh
yarn add eslint -D
npm install eslint -D
# 初始化一个配置文件
npx eslint --init
```
这样在项目的根目录就有一个 eslintrc.config.js 配置文件，

## 5 prettier 代码自动格式化

前端开发项目中会涉及到一些代码格式问题，比如函数括号后空格，CSS 格式，因此可以借助 Prettier 三方工具来实现团队代码的自动统一。

```sh
# 安装
npm add prettier -D
```
然后新建一个 .prettierrc.json 的配置文件，内容如下：

```json
{
  "printWidth": 100,         // 单行长度
  "tabWidth": 2,             // 缩进长度
  "useTabs": false,          // 使用空格代替tab缩进
  "semi": true,              // 句末使用分号
  "singleQuote": true,       // 使用单引号
  "bracketSpacing": true,    // 在对象前后添加空格-eg: { foo: bar }
  "arrowParens": "avoid"     // 单参数箭头函数参数周围使用圆括号 e.g. (x) => x
}
```


## Husky Git 提交约束

当然除了手动格式化，如果开发者没有格式化就提交代码到远程了怎么办，为此，引入 Husky 作为 Git commit 提交前做一个自动格式化暂存区内的文件，以及校验是否符合 Eslint 规则。

与此同时，还需要将用户的 git commit message 规范，可以引入 commitlint 工具，用于校验提交的 message 格式是否符合规范

```sh
yarn add husky@3.1.0 -D
yarn add lint-staged -D
yarn add @commitlint/cli -D
yarn add @commitlint/config-conventional -D
```
然后在 package.json 文件中新增如下内容：

```json
"husky": {
  "hooks": {
    "pre-commit": "lint-staged",
    "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
  }
},
"commitlint": {
  "extends": [
    "@commitlint/config-conventional"
  ]
},
"lint-staged": {
  "*.{ts,js}": [
    "node --max_old_space_size=8192 ./node_modules/.bin/prettier -w",
    "node --max_old_space_size=8192 ./node_modules/.bin/eslint --fix --color",
    "git add"
  ]
},
```

这样配置好了后，开发者在 git commit 时，会首先调用 lint-staged 字段中命令，首先是 prettier 格式化，然后是 ESlint 校验并修复，然后将修改后的文件存入暂存区。

然后是校验 commit message 是否符合规范，符合规范后才会成功 commit。

## 预发包前升级版本并构建

为了实现快捷发包，可以在 package.json 文件的 scripts 字段添加快捷命令，用于规范发包。

添加如下命令，可以在发包（执行 npm publish 命令）之前首先会提示升级包编译，然后 build 构建出产物

```sh
"prepublish": "yarn version && yarn build"
```

## 完成初始化

由于是开源库，所以咱把 package.json -> license 字段的值改为 MIT，可以参考：什么是 MIT 协议[4] 。

初始化的收尾工作就是将当前的变动存入 Git 记录中并关联远程仓库。

Git 远程同名仓库的创建就不赘述了，可以参考：创建仓库 - Github Docs[5]

然后我们将本地的代码 staged 并 commit

```sh
git init
git add -A
git commit -m "feat: init"

# 关联远程仓库并将本地代码提交到远程仓库：
git remote add origin "https://xxxx.com/xx/xx.git"
git push -u origin master
```
此后，我们可以把当前仓库作为一个开发支持 Typescript 的 NPM 包的模板仓库，后续开发新的 NPM 包，只需要克隆当前模板，然后再根据需要修改配置，新增 rollup 编译插件等就可以啦！

## 总结

梳理了在初始化构建一个工程项目中需要做的事情，涉及打包构建、开发、Git、发包的内容，从 0 到 1 愉快地完成了项目的初始化，后续就可以更加愉快地开发了。

TODO： 可以发现这种项目初始环境既然是相同的，可以赋予它模板的概念，那么是不是可以再动手写一个属于自己团队的开发脚手架（CLI），用于初始化项目？

### 提交库

npm publish

## 其他

```sh
npm install --save-dev rimraf         # 删除 文件夹
npm install --save-dev cross-env      # 跨平台设置环境变量 (可以在 package.json 的 调试脚本 script 中设置)
npm install --save-dev npm-run-all    # 
npm uninstall --save-dev ts-node
```

```sh
# package scripts 选项
"build:test": "cross-env NODE_ENV=development modulePath=sfx-meteojs input=src/index.ts rollup --config rollup/rollup.config.js",
```

上面的脚本可以设置 NODE_ENV, modulePath, input 三个环境变量, 通过
```sh
const node_env = process.env.NODE_ENV;
const module_path = process.env.modulePath;
const input = process.env.input;
```
可获取

### issue

发布控制发布目录

## 参考资料
[1]Rollup.js: https://rollupjs.org/guide/en/

[2]Configuration File - Prettier: https://prettier.io/docs/en/configuration.html

[3]Conventional Commits: https://www.conventionalcommits.org/en/v1.0.0/

[4]MIT 协议 -wikipedia.org: https://zh.wikipedia.org/wiki/MIT%E8%A8%B1%E5%8F%AF%E8%AD%89

[5]创建仓库 - Github Docs: https://docs.github.com/cn/get-started/quickstart/create-a-repo