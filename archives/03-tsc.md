# TSC

```sh
npm install -g typescript
npm install -g tslib
```

1、编写代码

```typescript
// app.ts
class HelloWorld {
    constructor() {
    }

    say() {
        console.log("Hello World!")
    }
}
new HelloWorld().say();
```

2、编译

```sh
tsc app.ts
tsc -d app.ts
```

会生成 app.js

3、运行

```sh
node app.js
```

会在控制台输出 "Hello World!".

ok

## ts-node 

```sh
npm i ts-node -g
ts-node app.ts
```

ts-node 会直接俄编译并在 node 中执行 (不会生成app.js)




