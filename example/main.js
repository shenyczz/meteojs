/* eslint-disable @typescript-eslint/no-require-imports */
console.log("在浏览器端 type = text/javascript 环境里使用 sfx-utils");
console.log("需要 require.js 模块支持");
console.log("");

// 配置 require
require.config({
    baseUrl: "mylibs",
    paths: {
        amd: "index.amd",
        umd: "index.umd",
        utils: "sfx-utils.min" // 没有扩展名
    }
});

//
// require(["./mylibs/index.amd.js"], (sfxUils) => test_color(sfxUils));
require(["amd", "umd", "utils"], (amd, umd, utils) => {
    test_amd(amd);
    test_umd(umd);
    test_utils(utils);
});

function test_amd(amd) {
    amd.strings.getPrefixString("abc");
}

function test_umd(umd) {
    umd.strings.getPrefixString("abc");
}

function test_utils(utils) {
    console.log("\t test_color ...");
    const color = new utils.Color(0.8);
    console.log(`\t color.toBytes = ${color.toBytes()}`);
    console.log(`\t color.toString() = ${color.toString()}`);
    console.log(`\t color.toCssColorString() = ${color.toCssColorString()}`);
    console.log(`\t color.toCssHexString() = ${color.toCssHexString()}`);
}
