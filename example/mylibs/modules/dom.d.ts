export declare function getElementSize(element: HTMLElement): {
    x: number;
    y: number;
    width: number;
    height: number;
};
export declare function hasClass(element: Element, cls: string): boolean;
//# sourceMappingURL=dom.d.ts.map