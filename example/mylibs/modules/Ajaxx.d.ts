export declare class Ajax {
    private constructor();
    protected static Fetch(url: string, callback?: Callback): void;
    static fetchJson(url: string, _options?: any): Promise<any>;
    static fetchText(url: string, _options?: any): Promise<string>;
    static fetchBlob(url: string, _options?: any): Promise<Blob>;
    static fetchArrayBuffer(url: string, _options?: any): Promise<ArrayBuffer>;
}
export default Ajax;
//# sourceMappingURL=Ajaxx.d.ts.map