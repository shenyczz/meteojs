export declare function isString(val: any): boolean;
export declare function isEmpty(val: string): boolean;
export declare function getPrefixString(str: string, prefix?: string): string;
//# sourceMappingURL=strings.d.ts.map