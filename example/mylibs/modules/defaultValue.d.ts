export declare function defaultValue(a: any, b: any): any;
export declare namespace defaultValue {
    var EMPTY_OBJECT: Readonly<{}>;
}
export default defaultValue;
//# sourceMappingURL=defaultValue.d.ts.map