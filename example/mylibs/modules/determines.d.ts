export declare function isString(val: any): val is string;
export declare function isNumber(val: any): val is number;
export declare function isBigint(val: any): val is bigint;
export declare function isBoolean(val: any): val is boolean;
export declare function isSymbol(val: any): val is symbol;
export declare function isUndefined(val: any): val is undefined;
export declare function isObject(val: any): boolean;
export declare function isFunction(val: any): boolean;
export declare function isNull(val: any): boolean;
export declare function isDefined(val: any): boolean;
export declare function isLeapYear(year: number): boolean;
//# sourceMappingURL=determines.d.ts.map