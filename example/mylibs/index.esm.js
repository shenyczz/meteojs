/**
 * @name        sfx-utils
 * @verions     1.0.0
 * @author      shenyc
 * @email       shenyczz@163.com
 * @license     MIT
 * @buildtime   Thu Oct 31 2024 15:13:37 GMT+0800 (中国标准时间)
 * @copyright   Copy right (c) 2020 - 2024. All rights reserved.
 *
******************************************************************************/
const test = () => console.log("This is common.ts");

var common = /*#__PURE__*/Object.freeze({
    __proto__: null,
    test: test
});

function isString$1(val) {
    return typeof val === "string";
}
function isNumber(val) {
    return typeof val === "number";
}
function isBigint(val) {
    return typeof val === "bigint";
}
function isBoolean(val) {
    return typeof val === "boolean";
}
function isSymbol(val) {
    return typeof val === "symbol";
}
function isUndefined(val) {
    return typeof val === "undefined";
}
function isObject(val) {
    return typeof val === "object";
}
function isFunction(val) {
    return typeof val === "function";
}
function isNull(val) {
    return isObject(val) && val === null;
}
function isDefined(val) {
    return !isNull(val) && !isUndefined(val);
}
function isLeapYear(year) {
    if (year === null || isNaN(year)) {
        throw "year is required and must be a number.";
    }
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

var determines = /*#__PURE__*/Object.freeze({
    __proto__: null,
    isBigint: isBigint,
    isBoolean: isBoolean,
    isDefined: isDefined,
    isFunction: isFunction,
    isLeapYear: isLeapYear,
    isNull: isNull,
    isNumber: isNumber,
    isObject: isObject,
    isString: isString$1,
    isSymbol: isSymbol,
    isUndefined: isUndefined
});

function getElementSize(element) {
    const rect = element.getBoundingClientRect();
    return {
        x: rect.x,
        y: rect.y,
        width: rect.width,
        height: rect.height
    };
}
function hasClass(element, cls) {
    const xxx = element.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"));
    return null !== xxx;
}

var dom = /*#__PURE__*/Object.freeze({
    __proto__: null,
    getElementSize: getElementSize,
    hasClass: hasClass
});

const foo = (i, fn) => {
    if (i == 0) {
        fn((x) => x);
    }
};
class ABC {
    constructor() {
        this.foo = (x) => x;
    }
}

var miscellaneous = /*#__PURE__*/Object.freeze({
    __proto__: null,
    ABC: ABC,
    foo: foo
});

function isString(val) {
    return isDefined(val) && "string" === typeof val;
}
function isEmpty(val) {
    let is_empty = true;
    if (isString(val)) {
        is_empty = val.replace(/(^s*)|(s*$)/g, "").length === 0;
    }
    return is_empty;
}
function getPrefixString(str, prefix = "") {
    const str_prefix = isEmpty(prefix) ? "sfx-prefix" : prefix;
    return `${str_prefix}-${str}`;
}

var strings = /*#__PURE__*/Object.freeze({
    __proto__: null,
    getPrefixString: getPrefixString,
    isEmpty: isEmpty,
    isString: isString
});

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol, Iterator */


function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

class Ajax {
    constructor() { }
    static Fetch(url, callback) {
        fetch(url)
            .then((resp) => resp.json())
            .then((jdata) => {
            if (callback)
                callback(jdata);
        })
            .catch((error) => {
            console.log(`${url} - ${error}`);
        });
    }
    static fetchJson(url, _options) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(url);
            return yield response.json();
        });
    }
    static fetchText(url, _options) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(url);
            return yield response.text();
        });
    }
    static fetchBlob(url, _options) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(url);
            return yield response.blob();
        });
    }
    static fetchArrayBuffer(url, _options) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield fetch(url);
            return yield response.arrayBuffer();
        });
    }
}

function defaultValue(a, b) {
    if (a !== undefined && a !== null) {
        return a;
    }
    return b;
}
defaultValue.EMPTY_OBJECT = Object.freeze({});

class Color {
    constructor(red, green, blue, alpha) {
        this.red = defaultValue(red, 1.0);
        this.green = defaultValue(green, 1.0);
        this.blue = defaultValue(blue, 1.0);
        this.alpha = defaultValue(alpha, 1.0);
    }
    clone() {
        return new Color(this.red, this.green, this.blue, this.alpha);
    }
    toBytes() {
        const red = Color.floatToByte(this.red);
        const green = Color.floatToByte(this.green);
        const blue = Color.floatToByte(this.blue);
        const alpha = Color.floatToByte(this.alpha);
        return [red, green, blue, alpha];
    }
    toString() {
        return `(${this.red}, ${this.green}, ${this.blue}, ${this.alpha})`;
    }
    toCssColorString() {
        const red = Color.floatToByte(this.red);
        const green = Color.floatToByte(this.green);
        const blue = Color.floatToByte(this.blue);
        if (this.alpha === 1) {
            return `rgb(${red},${green},${blue})`;
        }
        return `rgba(${red},${green},${blue},${this.alpha})`;
    }
    toCssHexString() {
        let r = Color.floatToByte(this.red).toString(16);
        if (r.length < 2) {
            r = `0${r}`;
        }
        let g = Color.floatToByte(this.green).toString(16);
        if (g.length < 2) {
            g = `0${g}`;
        }
        let b = Color.floatToByte(this.blue).toString(16);
        if (b.length < 2) {
            b = `0${b}`;
        }
        if (this.alpha < 1) {
            let hexAlpha = Color.floatToByte(this.alpha).toString(16);
            if (hexAlpha.length < 2) {
                hexAlpha = `0${hexAlpha}`;
            }
            return `#${r}${g}${b}${hexAlpha}`;
        }
        return `#${r}${g}${b}`;
    }
    static fromBytes(red, green, blue, alpha) {
        red = Color.byteToFloat(defaultValue(red, 255.0));
        green = Color.byteToFloat(defaultValue(green, 255.0));
        blue = Color.byteToFloat(defaultValue(blue, 255.0));
        alpha = Color.byteToFloat(defaultValue(alpha, 255.0));
        return new Color(red, green, blue, alpha);
    }
    static fromRgba(rgba) {
        Color.scratchUint32Array[0] = rgba;
        return Color.fromBytes(Color.scratchUint8Array[0], Color.scratchUint8Array[1], Color.scratchUint8Array[2], Color.scratchUint8Array[3]);
    }
    static fromAlpha(baseColor, alpha) {
        const result = baseColor.clone();
        result.alpha = alpha;
        return result;
    }
    static pack(value, array, startingIndex = 0) {
        startingIndex = defaultValue(startingIndex, 0);
        array[startingIndex++] = value.red;
        array[startingIndex++] = value.green;
        array[startingIndex++] = value.blue;
        array[startingIndex] = value.alpha;
        return array;
    }
    static unpack(array, startingIndex = 0) {
        const red = array[startingIndex++];
        const green = array[startingIndex++];
        const blue = array[startingIndex++];
        const alpha = array[startingIndex];
        return new Color(red, green, blue, alpha);
    }
    static equals(left, right) {
        return (left === right ||
            (isDefined(left) &&
                isDefined(right) &&
                left.red === right.red &&
                left.green === right.green &&
                left.blue === right.blue &&
                left.alpha === right.alpha));
    }
    static fromHsl(hue, saturation, lightness, alpha) {
        hue = defaultValue(hue, 0.0) % 1.0;
        saturation = defaultValue(saturation, 0.0);
        lightness = defaultValue(lightness, 0.0);
        alpha = defaultValue(alpha, 1.0);
        let red = lightness;
        let green = lightness;
        let blue = lightness;
        if (saturation !== 0) {
            let m2 = 0;
            if (lightness < 0.5) {
                m2 = lightness * (1 + saturation);
            }
            else {
                m2 = lightness + saturation - lightness * saturation;
            }
            const m1 = 2.0 * lightness - m2;
            red = Color.hue2rgb(m1, m2, hue + 1 / 3);
            green = Color.hue2rgb(m1, m2, hue);
            blue = Color.hue2rgb(m1, m2, hue - 1 / 3);
        }
        return new Color(red, green, blue, alpha);
    }
    static fromCssColorString(color, defaultColor = new Color()) {
        color = color.trim();
        let matches = Color.rgbaMatcher.exec(color);
        if (matches !== null) {
            const red = parseInt(matches[1], 16) / 15;
            const green = parseInt(matches[2], 16) / 15.0;
            const blue = parseInt(matches[3], 16) / 15.0;
            const alpha = parseInt(defaultValue(matches[4], "f"), 16) / 15.0;
            return new Color(red, green, blue, alpha);
        }
        matches = Color.rrggbbaaMatcher.exec(color);
        if (matches !== null) {
            const red = parseInt(matches[1], 16) / 255.0;
            const green = parseInt(matches[2], 16) / 255.0;
            const blue = parseInt(matches[3], 16) / 255.0;
            const alpha = parseInt(defaultValue(matches[4], "ff"), 16) / 255.0;
            return new Color(red, green, blue, alpha);
        }
        matches = Color.rgbParenthesesMatcher.exec(color);
        if (matches !== null) {
            const red = parseFloat(matches[1]) / ("%" === matches[1].substr(-1) ? 100.0 : 255.0);
            const green = parseFloat(matches[2]) / ("%" === matches[2].substr(-1) ? 100.0 : 255.0);
            const blue = parseFloat(matches[3]) / ("%" === matches[3].substr(-1) ? 100.0 : 255.0);
            const alpha = parseFloat(defaultValue(matches[4], "1.0"));
            return new Color(red, green, blue, alpha);
        }
        matches = Color.hslParenthesesMatcher.exec(color);
        if (matches !== null) {
            return Color.fromHsl(parseFloat(matches[1]) / 360.0, parseFloat(matches[2]) / 100.0, parseFloat(matches[3]) / 100.0, parseFloat(defaultValue(matches[4], "1.0")));
        }
        return defaultColor;
    }
    static byteToFloat(byte) {
        return byte / 255.0;
    }
    static floatToByte(val) {
        return val === 1.0 ? 255.0 : (val * 256.0) | 0;
    }
    static hue2rgb(m1, m2, h) {
        if (h < 0) {
            h += 1;
        }
        if (h > 1) {
            h -= 1;
        }
        if (h * 6 < 1) {
            return m1 + (m2 - m1) * 6 * h;
        }
        if (h * 2 < 1) {
            return m2;
        }
        if (h * 3 < 2) {
            return m1 + (m2 - m1) * (2 / 3 - h) * 6;
        }
        return m1;
    }
}
(function (Color) {
    Color.rgbaMatcher = /^#([0-9a-f])([0-9a-f])([0-9a-f])([0-9a-f])?$/i;
    Color.rrggbbaaMatcher = /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})?$/i;
    Color.rgbParenthesesMatcher = /^rgba?\s*\(\s*([0-9.]+%?)\s*[,\s]+\s*([0-9.]+%?)\s*[,\s]+\s*([0-9.]+%?)(?:\s*[,\s/]+\s*([0-9.]+))?\s*\)$/i;
    Color.hslParenthesesMatcher = /^hsla?\s*\(\s*([0-9.]+)\s*[,\s]+\s*([0-9.]+%)\s*[,\s]+\s*([0-9.]+%)(?:\s*[,\s/]+\s*([0-9.]+))?\s*\)$/i;
    const scratchArrayBuffer = new ArrayBuffer(4);
    Color.scratchUint8Array = new Uint8Array(scratchArrayBuffer);
    Color.scratchUint32Array = new Uint32Array(scratchArrayBuffer);
    Color.WHITE = Object.freeze(new Color());
    Color.ALICEBLUE = Object.freeze(Color.fromCssColorString("#F0F8FF"));
    Color.ANTIQUEWHITE = Object.freeze(Color.fromCssColorString("#FAEBD7"));
    Color.AQUA = Object.freeze(Color.fromCssColorString("#00FFFF"));
    Color.AQUAMARINE = Object.freeze(Color.fromCssColorString("#7FFFD4"));
    Color.AZURE = Object.freeze(Color.fromCssColorString("#F0FFFF"));
    Color.BEIGE = Object.freeze(Color.fromCssColorString("#F5F5DC"));
    Color.BISQUE = Object.freeze(Color.fromCssColorString("#FFE4C4"));
    Color.BLACK = Object.freeze(Color.fromCssColorString("#000000"));
    Color.BLANCHEDALMOND = Object.freeze(Color.fromCssColorString("#FFEBCD"));
    Color.BLUE = Object.freeze(Color.fromCssColorString("#0000FF"));
    Color.BLUEVIOLET = Object.freeze(Color.fromCssColorString("#8A2BE2"));
    Color.BROWN = Object.freeze(Color.fromCssColorString("#A52A2A"));
    Color.BURLYWOOD = Object.freeze(Color.fromCssColorString("#DEB887"));
    Color.CADETBLUE = Object.freeze(Color.fromCssColorString("#5F9EA0"));
    Color.CHARTREUSE = Object.freeze(Color.fromCssColorString("#7FFF00"));
    Color.CHOCOLATE = Object.freeze(Color.fromCssColorString("#D2691E"));
    Color.CORAL = Object.freeze(Color.fromCssColorString("#FF7F50"));
    Color.CORNFLOWERBLUE = Object.freeze(Color.fromCssColorString("#6495ED"));
    Color.CORNSILK = Object.freeze(Color.fromCssColorString("#FFF8DC"));
    Color.CRIMSON = Object.freeze(Color.fromCssColorString("#DC143C"));
    Color.CYAN = Object.freeze(Color.fromCssColorString("#00FFFF"));
    Color.DARKBLUE = Object.freeze(Color.fromCssColorString("#00008B"));
    Color.DARKCYAN = Object.freeze(Color.fromCssColorString("#008B8B"));
    Color.DARKGOLDENROD = Object.freeze(Color.fromCssColorString("#B8860B"));
    Color.DARKGRAY = Object.freeze(Color.fromCssColorString("#A9A9A9"));
    Color.DARKGREEN = Object.freeze(Color.fromCssColorString("#006400"));
    Color.DARKGREY = Color.DARKGRAY;
    Color.DARKKHAKI = Object.freeze(Color.fromCssColorString("#BDB76B"));
    Color.DARKMAGENTA = Object.freeze(Color.fromCssColorString("#8B008B"));
})(Color || (Color = {}));
var Color$1 = Color;

export { Ajax, Color$1 as Color, common, defaultValue, determines, dom, miscellaneous as mics, strings };
//# sourceMappingURL=index.esm.js.map
