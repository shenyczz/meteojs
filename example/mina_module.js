import * as utils from "./mylibs/index.esm.js";

console.log("在浏览器端 type = module 环境里使用 sfx-utils");

// test_color();

// function test_color() {
//     console.log("\t test_color ...");
//     const color = new utils.Color(0.8);
//     console.log(`\t color.toBytes = ${color.toBytes()}`);
//     console.log(`\t color.toString() = ${color.toString()}`);
//     console.log(`\t color.toCssColorString() = ${color.toCssColorString()}`);
//     console.log(`\t color.toCssHexString() = ${color.toCssHexString()}`);
// }
const url = "assets/w.json";
utils.Ajax.fetchJson(url).then((json) => console.log(json));
// utils.Ajax.fetchText(url).then((text) => console.log(text));
// utils.Ajax.fetchBlob(url).then((blob) => console.log(blob));
utils.Ajax.fetchArrayBuffer(url).then((buf) => console.log(buf));
