/**
 * @name        declares.d.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
export {};
// declare modules "rollup" = {}
declare global {
    /** 回调类型 */
    export type Callback = (option?: any) => any;
}
// ----------------------------------------------------------------------------
// {{@@@}}
