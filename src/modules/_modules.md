# 分类

-   诊断:  
    **determine**; judge; judgment; judgement; measure; decide; estimate; diagnosis; diagnose; size up;

-   检查
    **check**

-   字符串
    **strings**

-   杂项：  
    **miscellaneous**

-   混合：  
    **mixed**; hybrid; medley;
