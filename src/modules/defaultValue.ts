/**
 * @file        template.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
/**
 * Returns the first parameter if not undefined, otherwise the second parameter.
 * Useful for setting a default value for a parameter.
 *
 * @function
 *
 * @param {any} a
 * @param {any} b
 * @returns {any} Returns the first parameter if not undefined, otherwise the second parameter.
 *
 * @example
 * param = defaultValue(param, 'default');
 */
export function defaultValue(a: any, b: any): any {
    if (a !== undefined && a !== null) {
        return a;
    }
    return b;
}

/**
 * A frozen empty object that can be used as the default value for options passed as
 * an object literal.
 * @type {object}
 * @memberof defaultValue
 */
defaultValue.EMPTY_OBJECT = Object.freeze({});

export default defaultValue;
// ----------------------------------------------------------------------------
// {{@@@}}
