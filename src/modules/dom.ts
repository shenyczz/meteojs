/**
 * @file        dom.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
/**
 * 取得HTML 元素尺寸
 * @param {HTMLElement} [element] HTML 元素
 * @returns rect{ x, y, width, height }
 */
export function getElementSize(element: HTMLElement) {
    const rect = element.getBoundingClientRect();
    return {
        x: rect.x,
        y: rect.y,
        width: rect.width,
        height: rect.height
    };
}

/**
 * DOM 元素是否有 class 属性
 * @param element DOM 元素
 * @param cls 类名
 * @returns
 */
export function hasClass(element: Element, cls: string) {
    const xxx = element.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"));
    return null !== xxx;
}
// ----------------------------------------------------------------------------
// {{@@@}}
