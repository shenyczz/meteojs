/**
 * @name        zclass.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
/**
 * 用于测试的类
 */
export class CTest {
    constructor() {}

    hello() {
        console.log(`Hello class CTest !`);
    }
}
// ----------------------------------------------------------------------------
// {{@@@}}
