/**
 * @name        strings.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
import { isDefined } from "./determines";
// ----------------------------------------------------------------------------

/**
 * 是否字符串
 * @param val
 * @returns boolean
 */
export function isString(val: any) {
    return isDefined(val) && "string" === typeof val;
}

/**
 * 是否空字符串
 * @param val
 * @returns
 */
export function isEmpty(val: string) {
    let is_empty = true;

    if (isString(val)) {
        is_empty = val.replace(/(^s*)|(s*$)/g, "").length === 0;
    }

    return is_empty;
}

/**
 * 返回带前缀的字符串
 *
 * @param str 源字符串
 * @param prefix 前缀字符串
 * @returns 加前缀的字符串
 */
export function getPrefixString(str: string, prefix: string = "") {
    const str_prefix = isEmpty(prefix) ? "sfx-prefix" : prefix;
    return `${str_prefix}-${str}`;
}

// ----------------------------------------------------------------------------
// {{@@@}}
/**
 *
 * @param val 源字符串
 * @returns "sfx-prefix-val"
 */
