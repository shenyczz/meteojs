/**
 * @name        misc.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------

export const foo = (i: number, fn: Callback) => {
    if (i == 0) {
        fn((x: any) => x);
    }
};

export class ABC {
    public foo = (x: any) => x;
}

// ----------------------------------------------------------------------------
// {{@@@}}
