/**
 * @file        diagnos.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 * @description 诊断函数
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
/**
 * 是否字符串
 * @param {any} val
 * @returns
 */
export function isString(val: any) {
    return typeof val === "string";
}

/**
 * 是否数字
 * @param val
 * @returns
 */
export function isNumber(val: any) {
    return typeof val === "number";
}

/**
 * 是否大整形
 * @param val
 * @returns
 */
export function isBigint(val: any) {
    return typeof val === "bigint";
}

/**
 * 是否 boolean 类型
 * @param val
 * @returns
 */
export function isBoolean(val: any) {
    return typeof val === "boolean";
}

/**
 * 是否符号
 * @param val
 * @returns
 */
export function isSymbol(val: any) {
    return typeof val === "symbol";
}

/**
 * 是否未定义
 * @param val
 * @returns
 */
export function isUndefined(val: any) {
    return typeof val === "undefined";
}

/**
 * 是否 object 类型
 * @param val
 * @returns
 */
export function isObject(val: any) {
    return typeof val === "object";
}

/**
 * 是否 function 类型
 * @param val
 * @returns
 */
export function isFunction(val: any) {
    return typeof val === "function";
}

/**
 * 是否 null 类型
 * @param val
 * @returns
 */
export function isNull(val: any) {
    return isObject(val) && val === null;
}

/**
 * 判断对象是否定义
 *
 * @param val
 * @returns
 */
export function isDefined(val: any) {
    return !isNull(val) && !isUndefined(val);
}
// ----------------------------------------------------------------------------
/**
 * Determines if a given date is a leap year.
 *
 * @function isLeapYear
 *
 * @param {number} year The year to be tested.
 * @returns {boolean} True if <code>year</code> is a leap year.
 *
 * @example
 * const leapYear = isLeapYear(2000); // true
 */
export function isLeapYear(year: number): boolean {
    //
    if (year === null || isNaN(year)) {
        throw "year is required and must be a number.";
    }

    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

// ----------------------------------------------------------------------------
// {{@@@}}
