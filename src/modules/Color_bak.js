import Check from "./Check.js";
import defaultValue from "./defaultValue.js";
import defined from "./defined.js";
import FeatureDetection from "./FeatureDetection.js";
import CesiumMath from "./Math.js";






/**
 * Creates a random color using the provided options. For reproducible random colors, you should
 * call {@link CesiumMath#setRandomNumberSeed} once at the beginning of your application.
 *
 * @param {object} [options] Object with the following properties:
 * @param {number} [options.red] If specified, the red component to use instead of a randomized value.
 * @param {number} [options.minimumRed=0.0] The maximum red value to generate if none was specified.
 * @param {number} [options.maximumRed=1.0] The minimum red value to generate if none was specified.
 * @param {number} [options.green] If specified, the green component to use instead of a randomized value.
 * @param {number} [options.minimumGreen=0.0] The maximum green value to generate if none was specified.
 * @param {number} [options.maximumGreen=1.0] The minimum green value to generate if none was specified.
 * @param {number} [options.blue] If specified, the blue component to use instead of a randomized value.
 * @param {number} [options.minimumBlue=0.0] The maximum blue value to generate if none was specified.
 * @param {number} [options.maximumBlue=1.0] The minimum blue value to generate if none was specified.
 * @param {number} [options.alpha] If specified, the alpha component to use instead of a randomized value.
 * @param {number} [options.minimumAlpha=0.0] The maximum alpha value to generate if none was specified.
 * @param {number} [options.maximumAlpha=1.0] The minimum alpha value to generate if none was specified.
 * @param {Color} [result] The object to store the result in, if undefined a new instance will be created.
 * @returns {Color} The modified result parameter or a new instance if result was undefined.
 *
 * @exception {DeveloperError} minimumRed must be less than or equal to maximumRed.
 * @exception {DeveloperError} minimumGreen must be less than or equal to maximumGreen.
 * @exception {DeveloperError} minimumBlue must be less than or equal to maximumBlue.
 * @exception {DeveloperError} minimumAlpha must be less than or equal to maximumAlpha.
 *
 * @example
 * //Create a completely random color
 * const color = Cesium.Color.fromRandom();
 *
 * //Create a random shade of yellow.
 * const color1 = Cesium.Color.fromRandom({
 *     red : 1.0,
 *     green : 1.0,
 *     alpha : 1.0
 * });
 *
 * //Create a random bright color.
 * const color2 = Cesium.Color.fromRandom({
 *     minimumRed : 0.75,
 *     minimumGreen : 0.75,
 *     minimumBlue : 0.75,
 *     alpha : 1.0
 * });
 */
Color.fromRandom = function (options, result) {
    options = defaultValue(options, defaultValue.EMPTY_OBJECT);

    let red = options.red;
    if (!defined(red)) {
        const minimumRed = defaultValue(options.minimumRed, 0);
        const maximumRed = defaultValue(options.maximumRed, 1.0);

        //>>includeStart('debug', pragmas.debug);
        Check.typeOf.number.lessThanOrEquals("minimumRed", minimumRed, maximumRed);
        //>>includeEnd('debug');

        red = minimumRed + CesiumMath.nextRandomNumber() * (maximumRed - minimumRed);
    }

    let green = options.green;
    if (!defined(green)) {
        const minimumGreen = defaultValue(options.minimumGreen, 0);
        const maximumGreen = defaultValue(options.maximumGreen, 1.0);

        //>>includeStart('debug', pragmas.debug);
        Check.typeOf.number.lessThanOrEquals("minimumGreen", minimumGreen, maximumGreen);
        //>>includeEnd('debug');
        green = minimumGreen + CesiumMath.nextRandomNumber() * (maximumGreen - minimumGreen);
    }

    let blue = options.blue;
    if (!defined(blue)) {
        const minimumBlue = defaultValue(options.minimumBlue, 0);
        const maximumBlue = defaultValue(options.maximumBlue, 1.0);

        //>>includeStart('debug', pragmas.debug);
        Check.typeOf.number.lessThanOrEquals("minimumBlue", minimumBlue, maximumBlue);
        //>>includeEnd('debug');

        blue = minimumBlue + CesiumMath.nextRandomNumber() * (maximumBlue - minimumBlue);
    }

    let alpha = options.alpha;
    if (!defined(alpha)) {
        const minimumAlpha = defaultValue(options.minimumAlpha, 0);
        const maximumAlpha = defaultValue(options.maximumAlpha, 1.0);

        //>>includeStart('debug', pragmas.debug);
        Check.typeOf.number.lessThanOrEquals("minumumAlpha", minimumAlpha, maximumAlpha);
        //>>includeEnd('debug');

        alpha = minimumAlpha + CesiumMath.nextRandomNumber() * (maximumAlpha - minimumAlpha);
    }

    if (!defined(result)) {
        return new Color(red, green, blue, alpha);
    }

    result.red = red;
    result.green = green;
    result.blue = blue;
    result.alpha = alpha;
    return result;
};


/**
 * The number of elements used to pack the object into an array.
 * @type {number}
 */
Color.packedLength = 4;


/**
 * @private
 */
Color.equalsArray = function (color, array, offset) {
    return color.red === array[offset] && color.green === array[offset + 1] && color.blue === array[offset + 2] && color.alpha === array[offset + 3];
};

/**
 * Returns <code>true</code> if this Color equals other componentwise within the specified epsilon.
 *
 * @param {Color} other The Color to compare for equality.
 * @param {number} [epsilon=0.0] The epsilon to use for equality testing.
 * @returns {boolean} <code>true</code> if the Colors are equal within the specified epsilon; otherwise, <code>false</code>.
 */
Color.prototype.equalsEpsilon = function (other, epsilon) {
    return (
        this === other || //
        (defined(other) && //
            Math.abs(this.red - other.red) <= epsilon && //
            Math.abs(this.green - other.green) <= epsilon && //
            Math.abs(this.blue - other.blue) <= epsilon && //
            Math.abs(this.alpha - other.alpha) <= epsilon)
    );
};



/**
 * Converts this color to a single numeric unsigned 32-bit RGBA value, using the endianness
 * of the system.
 *
 * @returns {number} A single numeric unsigned 32-bit RGBA value.
 *
 *
 * @example
 * const rgba = Cesium.Color.BLUE.toRgba();
 *
 * @see Color.fromRgba
 */
Color.prototype.toRgba = function () {
    // scratchUint32Array and scratchUint8Array share an underlying array buffer
    scratchUint8Array[0] = Color.floatToByte(this.red);
    scratchUint8Array[1] = Color.floatToByte(this.green);
    scratchUint8Array[2] = Color.floatToByte(this.blue);
    scratchUint8Array[3] = Color.floatToByte(this.alpha);
    return scratchUint32Array[0];
};

/**
 * Brightens this color by the provided magnitude.
 *
 * @param {number} magnitude A positive number indicating the amount to brighten.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 *
 * @example
 * const brightBlue = Cesium.Color.BLUE.brighten(0.5, new Cesium.Color());
 */
Color.prototype.brighten = function (magnitude, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.number("magnitude", magnitude);
    Check.typeOf.number.greaterThanOrEquals("magnitude", magnitude, 0.0);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    magnitude = 1.0 - magnitude;
    result.red = 1.0 - (1.0 - this.red) * magnitude;
    result.green = 1.0 - (1.0 - this.green) * magnitude;
    result.blue = 1.0 - (1.0 - this.blue) * magnitude;
    result.alpha = this.alpha;
    return result;
};

/**
 * Darkens this color by the provided magnitude.
 *
 * @param {number} magnitude A positive number indicating the amount to darken.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 *
 * @example
 * const darkBlue = Cesium.Color.BLUE.darken(0.5, new Cesium.Color());
 */
Color.prototype.darken = function (magnitude, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.number("magnitude", magnitude);
    Check.typeOf.number.greaterThanOrEquals("magnitude", magnitude, 0.0);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    magnitude = 1.0 - magnitude;
    result.red = this.red * magnitude;
    result.green = this.green * magnitude;
    result.blue = this.blue * magnitude;
    result.alpha = this.alpha;
    return result;
};

/**
 * Creates a new Color that has the same red, green, and blue components
 * as this Color, but with the specified alpha value.
 *
 * @param {number} alpha The new alpha component.
 * @param {Color} [result] The object onto which to store the result.
 * @returns {Color} The modified result parameter or a new Color instance if one was not provided.
 *
 * @example const translucentRed = Cesium.Color.RED.withAlpha(0.9);
 */
Color.prototype.withAlpha = function (alpha, result) {
    return Color.fromAlpha(this, alpha, result);
};

/**
 * Computes the componentwise sum of two Colors.
 *
 * @param {Color} left The first Color.
 * @param {Color} right The second Color.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.add = function (left, right, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("left", left);
    Check.typeOf.object("right", right);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = left.red + right.red;
    result.green = left.green + right.green;
    result.blue = left.blue + right.blue;
    result.alpha = left.alpha + right.alpha;
    return result;
};

/**
 * Computes the componentwise difference of two Colors.
 *
 * @param {Color} left The first Color.
 * @param {Color} right The second Color.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.subtract = function (left, right, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("left", left);
    Check.typeOf.object("right", right);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = left.red - right.red;
    result.green = left.green - right.green;
    result.blue = left.blue - right.blue;
    result.alpha = left.alpha - right.alpha;
    return result;
};

/**
 * Computes the componentwise product of two Colors.
 *
 * @param {Color} left The first Color.
 * @param {Color} right The second Color.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.multiply = function (left, right, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("left", left);
    Check.typeOf.object("right", right);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = left.red * right.red;
    result.green = left.green * right.green;
    result.blue = left.blue * right.blue;
    result.alpha = left.alpha * right.alpha;
    return result;
};

/**
 * Computes the componentwise quotient of two Colors.
 *
 * @param {Color} left The first Color.
 * @param {Color} right The second Color.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.divide = function (left, right, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("left", left);
    Check.typeOf.object("right", right);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = left.red / right.red;
    result.green = left.green / right.green;
    result.blue = left.blue / right.blue;
    result.alpha = left.alpha / right.alpha;
    return result;
};

/**
 * Computes the componentwise modulus of two Colors.
 *
 * @param {Color} left The first Color.
 * @param {Color} right The second Color.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.mod = function (left, right, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("left", left);
    Check.typeOf.object("right", right);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = left.red % right.red;
    result.green = left.green % right.green;
    result.blue = left.blue % right.blue;
    result.alpha = left.alpha % right.alpha;
    return result;
};

/**
 * Computes the linear interpolation or extrapolation at t between the provided colors.
 *
 * @param {Color} start The color corresponding to t at 0.0.
 * @param {Color} end The color corresponding to t at 1.0.
 * @param {number} t The point along t at which to interpolate.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.lerp = function (start, end, t, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("start", start);
    Check.typeOf.object("end", end);
    Check.typeOf.number("t", t);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = CesiumMath.lerp(start.red, end.red, t);
    result.green = CesiumMath.lerp(start.green, end.green, t);
    result.blue = CesiumMath.lerp(start.blue, end.blue, t);
    result.alpha = CesiumMath.lerp(start.alpha, end.alpha, t);
    return result;
};

/**
 * Multiplies the provided Color componentwise by the provided scalar.
 *
 * @param {Color} color The Color to be scaled.
 * @param {number} scalar The scalar to multiply with.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.multiplyByScalar = function (color, scalar, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("color", color);
    Check.typeOf.number("scalar", scalar);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = color.red * scalar;
    result.green = color.green * scalar;
    result.blue = color.blue * scalar;
    result.alpha = color.alpha * scalar;
    return result;
};

/**
 * Divides the provided Color componentwise by the provided scalar.
 *
 * @param {Color} color The Color to be divided.
 * @param {number} scalar The scalar to divide with.
 * @param {Color} result The object onto which to store the result.
 * @returns {Color} The modified result parameter.
 */
Color.divideByScalar = function (color, scalar, result) {
    //>>includeStart('debug', pragmas.debug);
    Check.typeOf.object("color", color);
    Check.typeOf.number("scalar", scalar);
    Check.typeOf.object("result", result);
    //>>includeEnd('debug');

    result.red = color.red / scalar;
    result.green = color.green / scalar;
    result.blue = color.blue / scalar;
    result.alpha = color.alpha / scalar;
    return result;
};









/**
 * An immutable Color instance initialized to CSS color #556B2F
 * <span class="colorSwath" style="background: #556B2F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKOLIVEGREEN = Object.freeze(Color.fromCssColorString("#556B2F"));

/**
 * An immutable Color instance initialized to CSS color #FF8C00
 * <span class="colorSwath" style="background: #FF8C00;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKORANGE = Object.freeze(Color.fromCssColorString("#FF8C00"));

/**
 * An immutable Color instance initialized to CSS color #9932CC
 * <span class="colorSwath" style="background: #9932CC;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKORCHID = Object.freeze(Color.fromCssColorString("#9932CC"));

/**
 * An immutable Color instance initialized to CSS color #8B0000
 * <span class="colorSwath" style="background: #8B0000;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKRED = Object.freeze(Color.fromCssColorString("#8B0000"));

/**
 * An immutable Color instance initialized to CSS color #E9967A
 * <span class="colorSwath" style="background: #E9967A;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKSALMON = Object.freeze(Color.fromCssColorString("#E9967A"));

/**
 * An immutable Color instance initialized to CSS color #8FBC8F
 * <span class="colorSwath" style="background: #8FBC8F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKSEAGREEN = Object.freeze(Color.fromCssColorString("#8FBC8F"));

/**
 * An immutable Color instance initialized to CSS color #483D8B
 * <span class="colorSwath" style="background: #483D8B;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKSLATEBLUE = Object.freeze(Color.fromCssColorString("#483D8B"));

/**
 * An immutable Color instance initialized to CSS color #2F4F4F
 * <span class="colorSwath" style="background: #2F4F4F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKSLATEGRAY = Object.freeze(Color.fromCssColorString("#2F4F4F"));

/**
 * An immutable Color instance initialized to CSS color #2F4F4F
 * <span class="colorSwath" style="background: #2F4F4F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKSLATEGREY = Color.DARKSLATEGRAY;

/**
 * An immutable Color instance initialized to CSS color #00CED1
 * <span class="colorSwath" style="background: #00CED1;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKTURQUOISE = Object.freeze(Color.fromCssColorString("#00CED1"));

/**
 * An immutable Color instance initialized to CSS color #9400D3
 * <span class="colorSwath" style="background: #9400D3;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DARKVIOLET = Object.freeze(Color.fromCssColorString("#9400D3"));

/**
 * An immutable Color instance initialized to CSS color #FF1493
 * <span class="colorSwath" style="background: #FF1493;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DEEPPINK = Object.freeze(Color.fromCssColorString("#FF1493"));

/**
 * An immutable Color instance initialized to CSS color #00BFFF
 * <span class="colorSwath" style="background: #00BFFF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DEEPSKYBLUE = Object.freeze(Color.fromCssColorString("#00BFFF"));

/**
 * An immutable Color instance initialized to CSS color #696969
 * <span class="colorSwath" style="background: #696969;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DIMGRAY = Object.freeze(Color.fromCssColorString("#696969"));

/**
 * An immutable Color instance initialized to CSS color #696969
 * <span class="colorSwath" style="background: #696969;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DIMGREY = Color.DIMGRAY;

/**
 * An immutable Color instance initialized to CSS color #1E90FF
 * <span class="colorSwath" style="background: #1E90FF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.DODGERBLUE = Object.freeze(Color.fromCssColorString("#1E90FF"));

/**
 * An immutable Color instance initialized to CSS color #B22222
 * <span class="colorSwath" style="background: #B22222;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.FIREBRICK = Object.freeze(Color.fromCssColorString("#B22222"));

/**
 * An immutable Color instance initialized to CSS color #FFFAF0
 * <span class="colorSwath" style="background: #FFFAF0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.FLORALWHITE = Object.freeze(Color.fromCssColorString("#FFFAF0"));

/**
 * An immutable Color instance initialized to CSS color #228B22
 * <span class="colorSwath" style="background: #228B22;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.FORESTGREEN = Object.freeze(Color.fromCssColorString("#228B22"));

/**
 * An immutable Color instance initialized to CSS color #FF00FF
 * <span class="colorSwath" style="background: #FF00FF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.FUCHSIA = Object.freeze(Color.fromCssColorString("#FF00FF"));

/**
 * An immutable Color instance initialized to CSS color #DCDCDC
 * <span class="colorSwath" style="background: #DCDCDC;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GAINSBORO = Object.freeze(Color.fromCssColorString("#DCDCDC"));

/**
 * An immutable Color instance initialized to CSS color #F8F8FF
 * <span class="colorSwath" style="background: #F8F8FF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GHOSTWHITE = Object.freeze(Color.fromCssColorString("#F8F8FF"));

/**
 * An immutable Color instance initialized to CSS color #FFD700
 * <span class="colorSwath" style="background: #FFD700;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GOLD = Object.freeze(Color.fromCssColorString("#FFD700"));

/**
 * An immutable Color instance initialized to CSS color #DAA520
 * <span class="colorSwath" style="background: #DAA520;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GOLDENROD = Object.freeze(Color.fromCssColorString("#DAA520"));

/**
 * An immutable Color instance initialized to CSS color #808080
 * <span class="colorSwath" style="background: #808080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GRAY = Object.freeze(Color.fromCssColorString("#808080"));

/**
 * An immutable Color instance initialized to CSS color #008000
 * <span class="colorSwath" style="background: #008000;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GREEN = Object.freeze(Color.fromCssColorString("#008000"));

/**
 * An immutable Color instance initialized to CSS color #ADFF2F
 * <span class="colorSwath" style="background: #ADFF2F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GREENYELLOW = Object.freeze(Color.fromCssColorString("#ADFF2F"));

/**
 * An immutable Color instance initialized to CSS color #808080
 * <span class="colorSwath" style="background: #808080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.GREY = Color.GRAY;

/**
 * An immutable Color instance initialized to CSS color #F0FFF0
 * <span class="colorSwath" style="background: #F0FFF0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.HONEYDEW = Object.freeze(Color.fromCssColorString("#F0FFF0"));

/**
 * An immutable Color instance initialized to CSS color #FF69B4
 * <span class="colorSwath" style="background: #FF69B4;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.HOTPINK = Object.freeze(Color.fromCssColorString("#FF69B4"));

/**
 * An immutable Color instance initialized to CSS color #CD5C5C
 * <span class="colorSwath" style="background: #CD5C5C;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.INDIANRED = Object.freeze(Color.fromCssColorString("#CD5C5C"));

/**
 * An immutable Color instance initialized to CSS color #4B0082
 * <span class="colorSwath" style="background: #4B0082;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.INDIGO = Object.freeze(Color.fromCssColorString("#4B0082"));

/**
 * An immutable Color instance initialized to CSS color #FFFFF0
 * <span class="colorSwath" style="background: #FFFFF0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.IVORY = Object.freeze(Color.fromCssColorString("#FFFFF0"));

/**
 * An immutable Color instance initialized to CSS color #F0E68C
 * <span class="colorSwath" style="background: #F0E68C;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.KHAKI = Object.freeze(Color.fromCssColorString("#F0E68C"));

/**
 * An immutable Color instance initialized to CSS color #E6E6FA
 * <span class="colorSwath" style="background: #E6E6FA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LAVENDER = Object.freeze(Color.fromCssColorString("#E6E6FA"));

/**
 * An immutable Color instance initialized to CSS color #FFF0F5
 * <span class="colorSwath" style="background: #FFF0F5;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LAVENDAR_BLUSH = Object.freeze(Color.fromCssColorString("#FFF0F5"));

/**
 * An immutable Color instance initialized to CSS color #7CFC00
 * <span class="colorSwath" style="background: #7CFC00;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LAWNGREEN = Object.freeze(Color.fromCssColorString("#7CFC00"));

/**
 * An immutable Color instance initialized to CSS color #FFFACD
 * <span class="colorSwath" style="background: #FFFACD;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LEMONCHIFFON = Object.freeze(Color.fromCssColorString("#FFFACD"));

/**
 * An immutable Color instance initialized to CSS color #ADD8E6
 * <span class="colorSwath" style="background: #ADD8E6;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTBLUE = Object.freeze(Color.fromCssColorString("#ADD8E6"));

/**
 * An immutable Color instance initialized to CSS color #F08080
 * <span class="colorSwath" style="background: #F08080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTCORAL = Object.freeze(Color.fromCssColorString("#F08080"));

/**
 * An immutable Color instance initialized to CSS color #E0FFFF
 * <span class="colorSwath" style="background: #E0FFFF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTCYAN = Object.freeze(Color.fromCssColorString("#E0FFFF"));

/**
 * An immutable Color instance initialized to CSS color #FAFAD2
 * <span class="colorSwath" style="background: #FAFAD2;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTGOLDENRODYELLOW = Object.freeze(Color.fromCssColorString("#FAFAD2"));

/**
 * An immutable Color instance initialized to CSS color #D3D3D3
 * <span class="colorSwath" style="background: #D3D3D3;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTGRAY = Object.freeze(Color.fromCssColorString("#D3D3D3"));

/**
 * An immutable Color instance initialized to CSS color #90EE90
 * <span class="colorSwath" style="background: #90EE90;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTGREEN = Object.freeze(Color.fromCssColorString("#90EE90"));

/**
 * An immutable Color instance initialized to CSS color #D3D3D3
 * <span class="colorSwath" style="background: #D3D3D3;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTGREY = Color.LIGHTGRAY;

/**
 * An immutable Color instance initialized to CSS color #FFB6C1
 * <span class="colorSwath" style="background: #FFB6C1;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTPINK = Object.freeze(Color.fromCssColorString("#FFB6C1"));

/**
 * An immutable Color instance initialized to CSS color #20B2AA
 * <span class="colorSwath" style="background: #20B2AA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTSEAGREEN = Object.freeze(Color.fromCssColorString("#20B2AA"));

/**
 * An immutable Color instance initialized to CSS color #87CEFA
 * <span class="colorSwath" style="background: #87CEFA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTSKYBLUE = Object.freeze(Color.fromCssColorString("#87CEFA"));

/**
 * An immutable Color instance initialized to CSS color #778899
 * <span class="colorSwath" style="background: #778899;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTSLATEGRAY = Object.freeze(Color.fromCssColorString("#778899"));

/**
 * An immutable Color instance initialized to CSS color #778899
 * <span class="colorSwath" style="background: #778899;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTSLATEGREY = Color.LIGHTSLATEGRAY;

/**
 * An immutable Color instance initialized to CSS color #B0C4DE
 * <span class="colorSwath" style="background: #B0C4DE;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTSTEELBLUE = Object.freeze(Color.fromCssColorString("#B0C4DE"));

/**
 * An immutable Color instance initialized to CSS color #FFFFE0
 * <span class="colorSwath" style="background: #FFFFE0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIGHTYELLOW = Object.freeze(Color.fromCssColorString("#FFFFE0"));

/**
 * An immutable Color instance initialized to CSS color #00FF00
 * <span class="colorSwath" style="background: #00FF00;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIME = Object.freeze(Color.fromCssColorString("#00FF00"));

/**
 * An immutable Color instance initialized to CSS color #32CD32
 * <span class="colorSwath" style="background: #32CD32;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LIMEGREEN = Object.freeze(Color.fromCssColorString("#32CD32"));

/**
 * An immutable Color instance initialized to CSS color #FAF0E6
 * <span class="colorSwath" style="background: #FAF0E6;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.LINEN = Object.freeze(Color.fromCssColorString("#FAF0E6"));

/**
 * An immutable Color instance initialized to CSS color #FF00FF
 * <span class="colorSwath" style="background: #FF00FF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MAGENTA = Object.freeze(Color.fromCssColorString("#FF00FF"));

/**
 * An immutable Color instance initialized to CSS color #800000
 * <span class="colorSwath" style="background: #800000;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MAROON = Object.freeze(Color.fromCssColorString("#800000"));

/**
 * An immutable Color instance initialized to CSS color #66CDAA
 * <span class="colorSwath" style="background: #66CDAA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMAQUAMARINE = Object.freeze(Color.fromCssColorString("#66CDAA"));

/**
 * An immutable Color instance initialized to CSS color #0000CD
 * <span class="colorSwath" style="background: #0000CD;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMBLUE = Object.freeze(Color.fromCssColorString("#0000CD"));

/**
 * An immutable Color instance initialized to CSS color #BA55D3
 * <span class="colorSwath" style="background: #BA55D3;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMORCHID = Object.freeze(Color.fromCssColorString("#BA55D3"));

/**
 * An immutable Color instance initialized to CSS color #9370DB
 * <span class="colorSwath" style="background: #9370DB;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMPURPLE = Object.freeze(Color.fromCssColorString("#9370DB"));

/**
 * An immutable Color instance initialized to CSS color #3CB371
 * <span class="colorSwath" style="background: #3CB371;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMSEAGREEN = Object.freeze(Color.fromCssColorString("#3CB371"));

/**
 * An immutable Color instance initialized to CSS color #7B68EE
 * <span class="colorSwath" style="background: #7B68EE;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMSLATEBLUE = Object.freeze(Color.fromCssColorString("#7B68EE"));

/**
 * An immutable Color instance initialized to CSS color #00FA9A
 * <span class="colorSwath" style="background: #00FA9A;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMSPRINGGREEN = Object.freeze(Color.fromCssColorString("#00FA9A"));

/**
 * An immutable Color instance initialized to CSS color #48D1CC
 * <span class="colorSwath" style="background: #48D1CC;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMTURQUOISE = Object.freeze(Color.fromCssColorString("#48D1CC"));

/**
 * An immutable Color instance initialized to CSS color #C71585
 * <span class="colorSwath" style="background: #C71585;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MEDIUMVIOLETRED = Object.freeze(Color.fromCssColorString("#C71585"));

/**
 * An immutable Color instance initialized to CSS color #191970
 * <span class="colorSwath" style="background: #191970;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MIDNIGHTBLUE = Object.freeze(Color.fromCssColorString("#191970"));

/**
 * An immutable Color instance initialized to CSS color #F5FFFA
 * <span class="colorSwath" style="background: #F5FFFA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MINTCREAM = Object.freeze(Color.fromCssColorString("#F5FFFA"));

/**
 * An immutable Color instance initialized to CSS color #FFE4E1
 * <span class="colorSwath" style="background: #FFE4E1;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MISTYROSE = Object.freeze(Color.fromCssColorString("#FFE4E1"));

/**
 * An immutable Color instance initialized to CSS color #FFE4B5
 * <span class="colorSwath" style="background: #FFE4B5;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.MOCCASIN = Object.freeze(Color.fromCssColorString("#FFE4B5"));

/**
 * An immutable Color instance initialized to CSS color #FFDEAD
 * <span class="colorSwath" style="background: #FFDEAD;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.NAVAJOWHITE = Object.freeze(Color.fromCssColorString("#FFDEAD"));

/**
 * An immutable Color instance initialized to CSS color #000080
 * <span class="colorSwath" style="background: #000080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.NAVY = Object.freeze(Color.fromCssColorString("#000080"));

/**
 * An immutable Color instance initialized to CSS color #FDF5E6
 * <span class="colorSwath" style="background: #FDF5E6;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.OLDLACE = Object.freeze(Color.fromCssColorString("#FDF5E6"));

/**
 * An immutable Color instance initialized to CSS color #808000
 * <span class="colorSwath" style="background: #808000;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.OLIVE = Object.freeze(Color.fromCssColorString("#808000"));

/**
 * An immutable Color instance initialized to CSS color #6B8E23
 * <span class="colorSwath" style="background: #6B8E23;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.OLIVEDRAB = Object.freeze(Color.fromCssColorString("#6B8E23"));

/**
 * An immutable Color instance initialized to CSS color #FFA500
 * <span class="colorSwath" style="background: #FFA500;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.ORANGE = Object.freeze(Color.fromCssColorString("#FFA500"));

/**
 * An immutable Color instance initialized to CSS color #FF4500
 * <span class="colorSwath" style="background: #FF4500;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.ORANGERED = Object.freeze(Color.fromCssColorString("#FF4500"));

/**
 * An immutable Color instance initialized to CSS color #DA70D6
 * <span class="colorSwath" style="background: #DA70D6;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.ORCHID = Object.freeze(Color.fromCssColorString("#DA70D6"));

/**
 * An immutable Color instance initialized to CSS color #EEE8AA
 * <span class="colorSwath" style="background: #EEE8AA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PALEGOLDENROD = Object.freeze(Color.fromCssColorString("#EEE8AA"));

/**
 * An immutable Color instance initialized to CSS color #98FB98
 * <span class="colorSwath" style="background: #98FB98;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PALEGREEN = Object.freeze(Color.fromCssColorString("#98FB98"));

/**
 * An immutable Color instance initialized to CSS color #AFEEEE
 * <span class="colorSwath" style="background: #AFEEEE;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PALETURQUOISE = Object.freeze(Color.fromCssColorString("#AFEEEE"));

/**
 * An immutable Color instance initialized to CSS color #DB7093
 * <span class="colorSwath" style="background: #DB7093;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PALEVIOLETRED = Object.freeze(Color.fromCssColorString("#DB7093"));

/**
 * An immutable Color instance initialized to CSS color #FFEFD5
 * <span class="colorSwath" style="background: #FFEFD5;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PAPAYAWHIP = Object.freeze(Color.fromCssColorString("#FFEFD5"));

/**
 * An immutable Color instance initialized to CSS color #FFDAB9
 * <span class="colorSwath" style="background: #FFDAB9;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PEACHPUFF = Object.freeze(Color.fromCssColorString("#FFDAB9"));

/**
 * An immutable Color instance initialized to CSS color #CD853F
 * <span class="colorSwath" style="background: #CD853F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PERU = Object.freeze(Color.fromCssColorString("#CD853F"));

/**
 * An immutable Color instance initialized to CSS color #FFC0CB
 * <span class="colorSwath" style="background: #FFC0CB;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PINK = Object.freeze(Color.fromCssColorString("#FFC0CB"));

/**
 * An immutable Color instance initialized to CSS color #DDA0DD
 * <span class="colorSwath" style="background: #DDA0DD;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PLUM = Object.freeze(Color.fromCssColorString("#DDA0DD"));

/**
 * An immutable Color instance initialized to CSS color #B0E0E6
 * <span class="colorSwath" style="background: #B0E0E6;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.POWDERBLUE = Object.freeze(Color.fromCssColorString("#B0E0E6"));

/**
 * An immutable Color instance initialized to CSS color #800080
 * <span class="colorSwath" style="background: #800080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.PURPLE = Object.freeze(Color.fromCssColorString("#800080"));

/**
 * An immutable Color instance initialized to CSS color #FF0000
 * <span class="colorSwath" style="background: #FF0000;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.RED = Object.freeze(Color.fromCssColorString("#FF0000"));

/**
 * An immutable Color instance initialized to CSS color #BC8F8F
 * <span class="colorSwath" style="background: #BC8F8F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.ROSYBROWN = Object.freeze(Color.fromCssColorString("#BC8F8F"));

/**
 * An immutable Color instance initialized to CSS color #4169E1
 * <span class="colorSwath" style="background: #4169E1;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.ROYALBLUE = Object.freeze(Color.fromCssColorString("#4169E1"));

/**
 * An immutable Color instance initialized to CSS color #8B4513
 * <span class="colorSwath" style="background: #8B4513;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SADDLEBROWN = Object.freeze(Color.fromCssColorString("#8B4513"));

/**
 * An immutable Color instance initialized to CSS color #FA8072
 * <span class="colorSwath" style="background: #FA8072;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SALMON = Object.freeze(Color.fromCssColorString("#FA8072"));

/**
 * An immutable Color instance initialized to CSS color #F4A460
 * <span class="colorSwath" style="background: #F4A460;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SANDYBROWN = Object.freeze(Color.fromCssColorString("#F4A460"));

/**
 * An immutable Color instance initialized to CSS color #2E8B57
 * <span class="colorSwath" style="background: #2E8B57;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SEAGREEN = Object.freeze(Color.fromCssColorString("#2E8B57"));

/**
 * An immutable Color instance initialized to CSS color #FFF5EE
 * <span class="colorSwath" style="background: #FFF5EE;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SEASHELL = Object.freeze(Color.fromCssColorString("#FFF5EE"));

/**
 * An immutable Color instance initialized to CSS color #A0522D
 * <span class="colorSwath" style="background: #A0522D;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SIENNA = Object.freeze(Color.fromCssColorString("#A0522D"));

/**
 * An immutable Color instance initialized to CSS color #C0C0C0
 * <span class="colorSwath" style="background: #C0C0C0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SILVER = Object.freeze(Color.fromCssColorString("#C0C0C0"));

/**
 * An immutable Color instance initialized to CSS color #87CEEB
 * <span class="colorSwath" style="background: #87CEEB;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SKYBLUE = Object.freeze(Color.fromCssColorString("#87CEEB"));

/**
 * An immutable Color instance initialized to CSS color #6A5ACD
 * <span class="colorSwath" style="background: #6A5ACD;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SLATEBLUE = Object.freeze(Color.fromCssColorString("#6A5ACD"));

/**
 * An immutable Color instance initialized to CSS color #708090
 * <span class="colorSwath" style="background: #708090;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SLATEGRAY = Object.freeze(Color.fromCssColorString("#708090"));

/**
 * An immutable Color instance initialized to CSS color #708090
 * <span class="colorSwath" style="background: #708090;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SLATEGREY = Color.SLATEGRAY;

/**
 * An immutable Color instance initialized to CSS color #FFFAFA
 * <span class="colorSwath" style="background: #FFFAFA;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SNOW = Object.freeze(Color.fromCssColorString("#FFFAFA"));

/**
 * An immutable Color instance initialized to CSS color #00FF7F
 * <span class="colorSwath" style="background: #00FF7F;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.SPRINGGREEN = Object.freeze(Color.fromCssColorString("#00FF7F"));

/**
 * An immutable Color instance initialized to CSS color #4682B4
 * <span class="colorSwath" style="background: #4682B4;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.STEELBLUE = Object.freeze(Color.fromCssColorString("#4682B4"));

/**
 * An immutable Color instance initialized to CSS color #D2B48C
 * <span class="colorSwath" style="background: #D2B48C;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.TAN = Object.freeze(Color.fromCssColorString("#D2B48C"));

/**
 * An immutable Color instance initialized to CSS color #008080
 * <span class="colorSwath" style="background: #008080;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.TEAL = Object.freeze(Color.fromCssColorString("#008080"));

/**
 * An immutable Color instance initialized to CSS color #D8BFD8
 * <span class="colorSwath" style="background: #D8BFD8;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.THISTLE = Object.freeze(Color.fromCssColorString("#D8BFD8"));

/**
 * An immutable Color instance initialized to CSS color #FF6347
 * <span class="colorSwath" style="background: #FF6347;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.TOMATO = Object.freeze(Color.fromCssColorString("#FF6347"));

/**
 * An immutable Color instance initialized to CSS color #40E0D0
 * <span class="colorSwath" style="background: #40E0D0;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.TURQUOISE = Object.freeze(Color.fromCssColorString("#40E0D0"));

/**
 * An immutable Color instance initialized to CSS color #EE82EE
 * <span class="colorSwath" style="background: #EE82EE;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.VIOLET = Object.freeze(Color.fromCssColorString("#EE82EE"));

/**
 * An immutable Color instance initialized to CSS color #F5DEB3
 * <span class="colorSwath" style="background: #F5DEB3;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.WHEAT = Object.freeze(Color.fromCssColorString("#F5DEB3"));

/**
 * An immutable Color instance initialized to CSS color #FFFFFF
 * <span class="colorSwath" style="background: #FFFFFF;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.WHITE = Object.freeze(Color.fromCssColorString("#FFFFFF"));

/**
 * An immutable Color instance initialized to CSS color #F5F5F5
 * <span class="colorSwath" style="background: #F5F5F5;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.WHITESMOKE = Object.freeze(Color.fromCssColorString("#F5F5F5"));

/**
 * An immutable Color instance initialized to CSS color #FFFF00
 * <span class="colorSwath" style="background: #FFFF00;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.YELLOW = Object.freeze(Color.fromCssColorString("#FFFF00"));

/**
 * An immutable Color instance initialized to CSS color #9ACD32
 * <span class="colorSwath" style="background: #9ACD32;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.YELLOWGREEN = Object.freeze(Color.fromCssColorString("#9ACD32"));

/**
 * An immutable Color instance initialized to CSS transparent.
 * <span class="colorSwath" style="background: transparent;"></span>
 *
 * @constant
 * @type {Color}
 */
Color.TRANSPARENT = Object.freeze(new Color(0, 0, 0, 0));
export default Color;
