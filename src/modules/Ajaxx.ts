/**
 * @file        ajax.ts
 * @author      shenyc
 * @date        2023-05-06 - ?
 * @copyright   Copy right (c) shenyc (shenyczz@163.com).
 *              All rights reserved.
 *
 ******************************************************************************/
// {{@@@}}
"use strict";
// ----------------------------------------------------------------------------
export class Ajax {
    private constructor() {}

    /**
     * 异步交互获取资源
     * @param url - 资源地址
     * @param callback - 回调函数
     */
    protected static Fetch(url: string, callback?: Callback): void {
        fetch(url)
            .then((resp) => resp.json())
            .then((jdata) => {
                if (callback) callback(jdata);
            })
            .catch((error) => {
                console.log(`${url} - ${error}`);
            });
    }

    public static async fetchJson(url: string, _options?: any): Promise<any> {
        const response = await fetch(url);
        return await response.json();
    }
    public static async fetchText(url: string, _options?: any): Promise<string> {
        const response = await fetch(url);
        return await response.text();
    }
    public static async fetchBlob(url: string, _options?: any): Promise<Blob> {
        const response = await fetch(url);
        return await response.blob();
    }
    public static async fetchArrayBuffer(url: string, _options?: any): Promise<ArrayBuffer> {
        const response = await fetch(url);
        return await response.arrayBuffer();
    }
}

// function ajax_fetch_bak(url: string, callback?: Callback) {
//     fetch(url)
//         .then((resp) => resp.json())
//         .then((jdata) => {
//             if (callback) callback(jdata);
//         })
//         .catch((error) => {
//             console.log(`${url} - ${error}`);
//         });
// }

export default Ajax;
// ----------------------------------------------------------------------------
// {{@@@}}
