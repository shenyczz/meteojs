// Config file for running Rollup in "normal" mode (non-watch)
// ----------------------------------------------------------------------------
import { defineConfig } from "rollup";
import { Input, Output, Plugins, display_env } from "./rollup.define.js";
// ----------------------------------------------------------------------------
display_env();
// ----------------------------------------------------------------------------
const config = {
    input: Input,
    output: Output,
    plugins: Plugins,
    treeshake: true,
};

export default defineConfig([config]);
