import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";

export default [
    { files: ["**/*.{js,mjs,cjs,ts}"] },
    { languageOptions: { globals: globals.browser } },
    pluginJs.configs.recommended,
    ...tseslint.configs.recommended,
    {
        rules: {
            "no-undef": ["off"],
            "@typescript-eslint/no-unused-vars": ["off"],
            "@typescript-eslint/no-explicit-any": ["off"], // 关闭不能使用 any 类型
            "@typescript-eslint/no-namespace": ["off"]
        }
    }
];
